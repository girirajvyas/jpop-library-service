package com.epam.jpop.libraryservice.service;

import java.util.List;
import com.epam.jpop.libraryservice.model.BookDto;

/**
 * Library service having all the calls for user and book service
 * 
 * @author giri
 *
 */
public interface LibraryBookService {

  List<BookDto> findAllBooks();

  BookDto findBookById(Long id);

  BookDto saveBook(BookDto bookDto);
  
  BookDto updateBook(Long id, BookDto bookDto);

  void deleteBookbyId(Long id);

}
