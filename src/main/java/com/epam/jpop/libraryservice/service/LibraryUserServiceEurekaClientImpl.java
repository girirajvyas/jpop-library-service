package com.epam.jpop.libraryservice.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epam.jpop.libraryservice.client.UserRestClient;
import com.epam.jpop.libraryservice.model.UserDto;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;

@Service("LibraryUserServiceEurekaClientImpl")
public class LibraryUserServiceEurekaClientImpl implements LibraryUserService {

  private EurekaClient eurekaClient;

  @Autowired
  private UserRestClient userRestClient;

  LibraryUserServiceEurekaClientImpl(EurekaClient eurekaClient) {
    this.eurekaClient = eurekaClient;
  }

  @Override
  public List<UserDto> findAllUsers() {
    return userRestClient.findAllUsers(getServiceURL());
  }

  @Override
  public UserDto findUserById(Long id) {
    return userRestClient.findUserById(id, getServiceURL());
  }

  @Override
  public UserDto saveUser(UserDto userDto) {
    return userRestClient.saveUser(userDto, getServiceURL());
  }

  @Override
  public UserDto updateUser(Long id, UserDto userDto) {
    return userRestClient.updateUser(id, userDto, getServiceURL());
  }

  @Override
  public void deleteUserbyId(Long id) {
    userRestClient.deleteUserbyId(id, getServiceURL());
  }

  private String getServiceURL() {
    InstanceInfo instanceInfo = eurekaClient.getNextServerFromEureka("USER-SERVICE", false);
    return instanceInfo.getHomePageUrl() + "v1/users";

  }
}
