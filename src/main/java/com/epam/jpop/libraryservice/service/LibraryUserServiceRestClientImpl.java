package com.epam.jpop.libraryservice.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epam.jpop.libraryservice.client.UserRestClient;
import com.epam.jpop.libraryservice.model.UserDto;

@Service("LibraryUserServiceRestClientImpl")
public class LibraryUserServiceRestClientImpl implements LibraryUserService {

  private static final String BASE_URL = "http://localhost:8080/v1/users";
  
  @Autowired
  private UserRestClient userRestClient;

  @Override
  public List<UserDto> findAllUsers() {
    return userRestClient.findAllUsers(BASE_URL);
  }

  @Override
  public UserDto findUserById(Long id) {
    return userRestClient.findUserById(id, BASE_URL);
  }

  @Override
  public UserDto saveUser(UserDto userDto) {
    return userRestClient.saveUser(userDto, BASE_URL);
  }

  @Override
  public UserDto updateUser(Long id, UserDto userDto) {
    return userRestClient.updateUser(id, userDto, BASE_URL);
  }

  @Override
  public void deleteUserbyId(Long id) {
    userRestClient.deleteUserbyId(id, BASE_URL);
  }
}
