package com.epam.jpop.libraryservice.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epam.jpop.libraryservice.client.UserFeignClient;
import com.epam.jpop.libraryservice.model.UserDto;

@Service("LibraryUserServiceFeignClientImpl")
public class LibraryUserServiceFeignClientImpl implements LibraryUserService {

  @Autowired
  private UserFeignClient userFeignClient;

  @Override
  public List<UserDto> findAllUsers() {
    return userFeignClient.findAllUsers();
  }

  @Override
  public UserDto findUserById(Long id) {
    return userFeignClient.findUserById(id);
  }

  @Override
  public UserDto saveUser(UserDto userDto) {
    return userFeignClient.saveUser(userDto);
  }

  @Override
  public UserDto updateUser(Long id, UserDto userDto) {
    return userFeignClient.updateUser(id, userDto);
  }

  @Override
  public void deleteUserbyId(Long id) {
    userFeignClient.deleteUserbyId(id);
  }
}
