package com.epam.jpop.libraryservice.service;

import java.util.List;
import com.epam.jpop.libraryservice.model.UserDto;

/**
 * Library service having all the calls for user and book service
 * 
 * @author giri
 *
 */
public interface LibraryUserService {

  List<UserDto> findAllUsers();

  UserDto findUserById(Long id);

  UserDto saveUser(UserDto user);

  UserDto updateUser(Long id, UserDto userDto);

  void deleteUserbyId(Long id);


}
