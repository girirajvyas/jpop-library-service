package com.epam.jpop.libraryservice.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epam.jpop.libraryservice.client.BookRestClient;
import com.epam.jpop.libraryservice.model.BookDto;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;

@Service("LibraryBookServiceEurekaClientImpl")
public class LibraryBookServiceEurekaClientImpl implements LibraryBookService {

  @Autowired
  private EurekaClient eurekaClient;

  @Autowired
  private BookRestClient bookRestClient;

  @Override
  public List<BookDto> findAllBooks() {
    return bookRestClient.findAllBooks(getServiceURL());
  }

  @Override
  public BookDto findBookById(Long id) {
    return bookRestClient.findBookById(id, getServiceURL());
  }

  @Override
  public BookDto saveBook(BookDto bookDto) {
    return bookRestClient.saveBook(bookDto, getServiceURL());
  }

  @Override
  public BookDto updateBook(Long id, BookDto bookDto) {
    return bookRestClient.updateBook(id, bookDto, getServiceURL());
  }

  @Override
  public void deleteBookbyId(Long id) {
    bookRestClient.deleteBookbyId(id, getServiceURL());
  }

  private String getServiceURL() {
    InstanceInfo instanceInfo = eurekaClient.getNextServerFromEureka("BOOK-SERVICE", false);
    return instanceInfo.getHomePageUrl() + "v1/books";
  }
}
