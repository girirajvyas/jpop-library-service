package com.epam.jpop.libraryservice.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epam.jpop.libraryservice.client.BookRestClient;
import com.epam.jpop.libraryservice.model.BookDto;

@Service("LibraryBookServiceRestClientImpl")
public class LibraryBookServiceRestClientImpl implements LibraryBookService {

  private static final String BASE_URL = "http://localhost:8080/v1/books";
  
  @Autowired
  private BookRestClient bookRestClient;
  
  @Override
  public List<BookDto> findAllBooks() {
    return bookRestClient.findAllBooks(BASE_URL);
  }

  @Override
  public BookDto findBookById(Long id) {
    return bookRestClient.findBookById(id, BASE_URL);
  }

  @Override
  public BookDto saveBook(BookDto bookDto) {
    return bookRestClient.saveBook(bookDto, BASE_URL);
  }

  @Override
  public BookDto updateBook(Long id, BookDto bookDto) {
    return bookRestClient.updateBook(id, bookDto, BASE_URL);
  }

  @Override
  public void deleteBookbyId(Long id) {
    bookRestClient.deleteBookbyId(id, BASE_URL);
  }

}
