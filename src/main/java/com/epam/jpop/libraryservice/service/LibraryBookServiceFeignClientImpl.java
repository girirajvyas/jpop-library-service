package com.epam.jpop.libraryservice.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epam.jpop.libraryservice.client.BookFeignClient;
import com.epam.jpop.libraryservice.model.BookDto;

@Service("LibraryBookServiceFeignClientImpl")
public class LibraryBookServiceFeignClientImpl implements LibraryBookService {

  @Autowired
  private BookFeignClient bookFeignClient;

  @Override
  public List<BookDto> findAllBooks() {
    return bookFeignClient.findAllBooks();
  }

  @Override
  public BookDto findBookById(Long id) {
    return bookFeignClient.findBookById(id);
  }

  @Override
  public BookDto saveBook(BookDto bookDto) {
    return bookFeignClient.saveBook(bookDto);
  }

  @Override
  public BookDto updateBook(Long id, BookDto bookDto) {
    return bookFeignClient.updateBook(id, bookDto);
  }

  @Override
  public void deleteBookbyId(Long id) {
    bookFeignClient.deleteBookbyId(id);
  }

}
