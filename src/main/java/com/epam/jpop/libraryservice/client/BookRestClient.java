package com.epam.jpop.libraryservice.client;

import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import com.epam.jpop.libraryservice.model.BookDto;

/**
 * Rest client implementation to communicate within services. Disadvantage here is we have to hard
 * code urls, port and every time port changes this needs to be updated
 * 
 * @author giri
 *
 */
@Component
public class BookRestClient {

  private RestTemplate restTemplate = new RestTemplate();

  public List<BookDto> findAllBooks(String baseUrl) {
    return Arrays.asList(restTemplate.getForObject(baseUrl + "", BookDto[].class));
  }

  public BookDto findBookById(Long id, String baseUrl) {
    return restTemplate.getForObject(baseUrl + "/" + id, BookDto.class);
  }

  public BookDto saveBook(BookDto bookDto, String baseUrl) {
    BookDto bookDtoResponse = restTemplate.postForObject(baseUrl, bookDto, BookDto.class);
    System.out.println(bookDtoResponse);
    return bookDtoResponse;
  }

  public BookDto updateBook(Long id, BookDto bookDto, String baseUrl) {
    restTemplate.put(baseUrl + "/" + id, bookDto);
    return bookDto;
  }

  public void deleteBookbyId(Long id, String baseUrl) {
    restTemplate.delete(baseUrl + "/" + id);
  }



}
