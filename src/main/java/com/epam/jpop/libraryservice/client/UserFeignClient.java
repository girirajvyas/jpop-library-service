package com.epam.jpop.libraryservice.client;

import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.epam.jpop.libraryservice.model.UserDto;

/**
 * 
 * @author giri
 *
 */
@FeignClient(name = "USER-SERVICE", fallback = UserFeignClientFallback.class)
public interface UserFeignClient {

  @GetMapping("v1/users")
  public List<UserDto> findAllUsers();

  @GetMapping("v1/users/{user_id}")
  public UserDto findUserById(@PathVariable("user_id") Long id);

  @PostMapping("v1/users")
  public UserDto saveUser(@RequestBody UserDto userDto);

  @PutMapping("v1/users/{user_id}")
  public UserDto updateUser(@PathVariable("user_id") Long id, @RequestBody UserDto userDto);

  @DeleteMapping("v1/users/{user_id}")
  public void deleteUserbyId(@PathVariable("user_id") Long id);

}
