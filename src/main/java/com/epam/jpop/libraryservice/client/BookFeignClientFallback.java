package com.epam.jpop.libraryservice.client;

import java.util.Collections;
import java.util.List;
import org.springframework.stereotype.Component;
import com.epam.jpop.libraryservice.model.BookDto;

@Component
public class BookFeignClientFallback implements BookFeignClient {

  @Override
  public List<BookDto> findAllBooks() {
    return Collections.emptyList();
  }

  @Override
  public BookDto findBookById(Long id) {
    return new BookDto((long)0, "Backup name", "Farzi Author", "Tragedy", "Short description");
  }

  @Override
  public BookDto saveBook(BookDto bookDto) {
    return new BookDto((long)0, "Backup name", "Farzi Author", "Tragedy", "Short description");
  }

  @Override
  public BookDto updateBook(Long id, BookDto bookDto) {
    return new BookDto((long)0, "Backup name", "Farzi Author", "Tragedy", "Short description");
  }

  @Override
  public void deleteBookbyId(Long id) {
    
  }

}
