package com.epam.jpop.libraryservice.client;

import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.epam.jpop.libraryservice.model.BookDto;

@FeignClient(name = "BOOK-SERVICE", fallback = BookFeignClientFallback.class)
public interface BookFeignClient {

  @GetMapping("/v1/books")
  public List<BookDto> findAllBooks();

  @GetMapping("/v1/books/{book_id}")
  public BookDto findBookById(@PathVariable("book_id") Long id);

  @PostMapping("/v1/books")
  public BookDto saveBook(@RequestBody BookDto bookDto);

  @PutMapping("/v1/books/{book_id}")
  public BookDto updateBook(@PathVariable("book_id") Long id, @RequestBody BookDto bookDto);

  @DeleteMapping("/v1/books/{book_id}")
  public void deleteBookbyId(@PathVariable("book_id") Long id);

}
