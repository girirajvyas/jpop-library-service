package com.epam.jpop.libraryservice.client;

import java.util.Collections;
import java.util.List;
import org.springframework.stereotype.Component;
import com.epam.jpop.libraryservice.model.UserDto;

@Component
public class UserFeignClientFallback implements UserFeignClient {

  @Override
  public List<UserDto> findAllUsers() {
    return Collections.emptyList();
  }

  @Override
  public UserDto findUserById(Long id) {
    return new UserDto((long) 0, "Farzi", "fallback last name", "hystrix@backupentity.com",
        "000000000");
  }

  @Override
  public UserDto saveUser(UserDto userDto) {
    return new UserDto((long) 0, "Farzi", "fallback last name", "hystrix@backupentity.com",
        "000000000");
  }

  @Override
  public UserDto updateUser(Long id, UserDto userDto) {
    return new UserDto((long) 0, "Farzi", "fallback last name", "hystrix@backupentity.com",
        "000000000");
  }

  @Override
  public void deleteUserbyId(Long id) {}

}
