package com.epam.jpop.libraryservice.client;

import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import com.epam.jpop.libraryservice.model.UserDto;

/**
 * Rest client implementation to communicate within services. Disadvantage here is we have to hard
 * code urls, port and every time port changes this needs to be updated
 * 
 * @author giri
 *
 */
@Component
public class UserRestClient {

  
  private RestTemplate restTemplate = new RestTemplate();

  public List<UserDto> findAllUsers(String baseUrl) {
    return Arrays.asList(restTemplate.getForObject(baseUrl + "", UserDto[].class));
  }

  public UserDto findUserById(Long id, String baseUrl) {
    return restTemplate.getForObject(baseUrl + "/" + id, UserDto.class);
  }

  public UserDto saveUser(UserDto userDto, String baseUrl) {
    UserDto userDtoResponse = restTemplate.postForObject(baseUrl, userDto, UserDto.class);
    System.out.println(userDtoResponse);
    return userDtoResponse;
  }

  public UserDto updateUser(Long id, UserDto userDto, String baseUrl) {
    restTemplate.put(baseUrl + "/" + id, userDto);
    return userDto;
  }

  public void deleteUserbyId(Long id, String baseUrl) {
    restTemplate.delete(baseUrl + "/" + id);
  }

}
