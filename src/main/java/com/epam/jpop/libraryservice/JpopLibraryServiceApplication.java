package com.epam.jpop.libraryservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.openfeign.EnableFeignClients;


@EnableCircuitBreaker
@EnableFeignClients
@SpringBootApplication
public class JpopLibraryServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(JpopLibraryServiceApplication.class, args);
  }

}
