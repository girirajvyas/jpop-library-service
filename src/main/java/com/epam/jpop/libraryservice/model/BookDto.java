package com.epam.jpop.libraryservice.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "All details about the Book. ")
public class BookDto {

  @ApiModelProperty(notes = "The database generated ID")
  private Long id;
  
  @ApiModelProperty(notes = "Book name")
  private String name;
  
  @ApiModelProperty(notes = "Author of the book")
  private String author;
  
  @ApiModelProperty(notes = "Genre of the book")
  private String category;
  
  @ApiModelProperty(notes = "Short summary")
  private String description;
 
}
