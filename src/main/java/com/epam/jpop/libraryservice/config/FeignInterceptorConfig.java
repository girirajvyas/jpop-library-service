package com.epam.jpop.libraryservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import feign.RequestInterceptor;

@Configuration
public class FeignInterceptorConfig {

  private static final String KEY = "123456";
  @Bean
  public RequestInterceptor requestInterceptor() {
    return requestTemplate -> {
        requestTemplate.header("API_KEY", KEY);
    };
  }
  
}
