package com.epam.jpop.libraryservice.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.epam.jpop.libraryservice.model.BookDto;
import com.epam.jpop.libraryservice.service.LibraryBookService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 
 * @author giri
 *
 */
@Api(value = "Library Service")
@RestController
@RequestMapping("/v1/lib")
public class LibraryBooksController {

  // @Qualifier("LibraryBookServiceRestClientImpl")
  // @Qualifier("LibraryBookServiceEurekaClientImpl")
  @Qualifier("LibraryBookServiceFeignClientImpl")
  @Autowired
  private LibraryBookService libraryBookService;

  @ApiOperation(value = "Find all the books", response = List.class)
  @GetMapping("/books")
  public List<BookDto> findAllBooks() {
    return libraryBookService.findAllBooks();
  }

  @ApiOperation(value = "Get book by id", response = BookDto.class)
  @GetMapping("/books/{book_id}")
  public ResponseEntity<BookDto> findBookById(
      @ApiParam(value = "book id by which user will be retrieved",
          required = true) @PathVariable("book_id") long id) {
    BookDto bookDto = libraryBookService.findBookById(id);

    return ResponseEntity.ok(bookDto);
  }

  @ApiOperation(value = "Add a book", response = BookDto.class)
  @PostMapping("/books")
  public ResponseEntity<BookDto> createBook(@ApiParam(value = "Book object store in database table",
      required = true) @RequestBody BookDto bookDto) {
    return ResponseEntity.status(HttpStatus.CREATED).body(libraryBookService.saveBook(bookDto));
  }

  @ApiOperation(value = "update a book", response = BookDto.class)
  @PutMapping("/books/{book_id}")
  public ResponseEntity<BookDto> updateBook(
      @ApiParam(value = "book id by which user will be retrieved",
          required = true) @PathVariable("book_id") long id,
      @ApiParam(value = "Book object store in database table",
          required = true) @RequestBody BookDto bookDto) {
    return ResponseEntity.ok().body(libraryBookService.updateBook(id, bookDto));
  }

  @ApiOperation(value = "delete a book", response = BookDto.class)
  @DeleteMapping("/books/{book_id}")
  public ResponseEntity<BookDto> deleteBook(
      @ApiParam(value = "book id by which user will be retrieved",
          required = true) @PathVariable("book_id") long id) {
    libraryBookService.deleteBookbyId(id);
    return ResponseEntity.ok().build();
  }

}
