package com.epam.jpop.libraryservice.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.epam.jpop.libraryservice.model.UserDto;
import com.epam.jpop.libraryservice.service.LibraryUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 
 * @author giri
 *
 */
@Api(value = "Library Service")
@RestController
@RequestMapping("/v1/lib/users")
public class LibraryUsersController {

  
  @Qualifier("LibraryUserServiceFeignClientImpl")
  @Autowired
  private LibraryUserService libraryUserService;

  @ApiOperation(value = "Find all the users", response = List.class)
  @GetMapping
  public List<UserDto> findAllUsers() {
    return libraryUserService.findAllUsers();
  }

  @ApiOperation(value = "Get user by id", response = UserDto.class)
  @GetMapping("/{user_id}")
  public ResponseEntity<UserDto> findUserById(
      @ApiParam(value = "user id by which user will be retrieved",
          required = true) @PathVariable("user_id") long id) {
    UserDto userDto = libraryUserService.findUserById(id);
    return ResponseEntity.ok(userDto);
  }

  @ApiOperation(value = "Add a user", response = UserDto.class)
  @PostMapping
  public ResponseEntity<UserDto> createUser(@ApiParam(value = "User object store in database table",
      required = true) @RequestBody UserDto userDto) {
    return ResponseEntity.status(HttpStatus.CREATED).body(libraryUserService.saveUser(userDto));
  }

  @ApiOperation(value = "update a user", response = UserDto.class)
  @PutMapping("/{user_id}")
  public ResponseEntity<UserDto> updateUser(
      @ApiParam(value = "user id by which user will be retrieved",
          required = true) @PathVariable("user_id") long id,
      @ApiParam(value = "user object store in database table",
          required = true) @RequestBody UserDto userDto) {
    return ResponseEntity.ok().body(libraryUserService.updateUser(id, userDto));
  }

  @ApiOperation(value = "delete a user", response = UserDto.class)
  @DeleteMapping("/{user_id}")
  public ResponseEntity<UserDto> deleteUser(
      @ApiParam(value = "user id by which user will be deleted",
          required = true) @PathVariable("user_id") long id) {
    libraryUserService.deleteUserbyId(id);
    return ResponseEntity.ok().build();
  }

}
